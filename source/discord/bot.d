/**
 * d-discord
 * A simple, easy to use Discord library for making bots written in 
 * the D programming language.
 * 
 * Author: Dhillon (dhilln)
 * License: See LICENSE for details.
**/

module discord.bot;
import vibe.d;

/**
 * Wrapper class for a bot. Handles gateway events by abstracting
 * away all of the glue code and provides event hooks.
**/
class Bot
{
    private string _token;

    public this(string token)
    {
        // Set the authentication token
        this._token = token;
    }
}